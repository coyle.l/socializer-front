import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable, BehaviorSubject } from "rxjs";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };
  private userSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {}

  get user(): Observable<any> {
    return this.userSubject.asObservable();
  }

  updateUser(userId: number) {
    console.log("auth serice: userId:", userId);
    if (!userId) {
      this.userSubject.next(null);
    } else {
      this.userService.getUser(userId).subscribe(data => {
        console.log("auth service: update user:", data);
        this.userSubject.next(data);
      });
    }
  }

  register(user: any): Observable<any> {
    console.log("auth service user", user);
    return this.httpClient.post(
      `${environment.apiUrl}auth/register`,
      user,
      this.httpOptions
    );
  }

  login(email: string, password: string): Observable<any> {
    return this.httpClient.post(`${environment.apiUrl}auth/login`, {
      email: email,
      password: password
    });
  }
}
