import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/shared-service/authentication.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard-sidebar",
  templateUrl: "./dashboard-sidebar.component.html",
  styleUrls: ["./dashboard-sidebar.component.scss"]
})
export class DashboardSidebarComponent implements OnInit {
  user: any = {};
  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }
  logout() {
    localStorage.removeItem("USER");
    this.authService.updateUser(null);
    this.router.navigate(["home"]);
  }
}
