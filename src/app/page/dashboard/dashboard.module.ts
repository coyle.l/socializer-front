import { NgModule, OnInit } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";
import { DashboardInfoComponent } from "./dashboard-info/dashboard-info.component";
import { DashboardSidebarComponent } from "./dashboard-sidebar/dashboard-sidebar.component";
import { SharedServiceModule } from "src/app/shared-service/shared-service.module";
import { DashboardPreferenceComponent } from "./dashboard-preference/dashboard-preference.component";
import { DashboardMonitorComponent } from "./dashboard-monitor/dashboard-monitor.component";

@NgModule({
  declarations: [
    DashboardComponent,
    DashboardInfoComponent,
    DashboardSidebarComponent,
    DashboardPreferenceComponent,
    DashboardMonitorComponent
  ],
  imports: [CommonModule, DashboardRoutingModule, SharedServiceModule]
})
export class DashboardModule implements OnInit {
  ngOnInit(): void {}
}
