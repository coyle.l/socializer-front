import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";
import { DashboardInfoComponent } from "./dashboard-info/dashboard-info.component";
import { DashboardPreferenceComponent } from "./dashboard-preference/dashboard-preference.component";
import { DashboardMonitorComponent } from "./dashboard-monitor/dashboard-monitor.component";

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "info", component: DashboardInfoComponent },
      { path: "preference", component: DashboardPreferenceComponent },
      { path: "monitor", component: DashboardMonitorComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
