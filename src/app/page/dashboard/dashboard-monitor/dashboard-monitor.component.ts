import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/shared-service/authentication.service";

@Component({
  selector: "app-dashboard-monitor",
  templateUrl: "./dashboard-monitor.component.html",
  styleUrls: ["./dashboard-monitor.component.scss"]
})
export class DashboardMonitorComponent implements OnInit {
  user: any = {};
  constructor(private authService: AuthenticationService) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }
}
