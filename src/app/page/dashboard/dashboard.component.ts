import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/shared-service/authentication.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  constructor(private authService: AuthenticationService) {}
  user: any = {};
  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }
}
