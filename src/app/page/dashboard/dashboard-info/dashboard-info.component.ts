import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/shared-service/authentication.service";

@Component({
  selector: "app-dashboard-info",
  templateUrl: "./dashboard-info.component.html",
  styleUrls: ["./dashboard-info.component.scss"]
})
export class DashboardInfoComponent implements OnInit {
  user: any = {};

  constructor(private authService: AuthenticationService) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
      console.log("dashboard info user", user);
      this.user = user;
    });
  }
}
