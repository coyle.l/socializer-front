import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPreferenceComponent } from './dashboard-preference.component';

describe('DashboardPreferenceComponent', () => {
  let component: DashboardPreferenceComponent;
  let fixture: ComponentFixture<DashboardPreferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPreferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
