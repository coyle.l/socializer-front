import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/shared-service/authentication.service";

@Component({
  selector: "app-dashboard-preference",
  templateUrl: "./dashboard-preference.component.html",
  styleUrls: ["./dashboard-preference.component.scss"]
})
export class DashboardPreferenceComponent implements OnInit {
  user: any = {};
  constructor(private authService: AuthenticationService) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }
}
