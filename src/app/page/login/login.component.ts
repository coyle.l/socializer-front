import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthenticationService } from "src/app/shared-service/authentication.service";
import * as jwtDecode from "jwt-decode";
import { Router } from "@angular/router";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  formValue: any;
  user: any;
  payload: any;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }

  get form() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.loginForm.invalid) {
      this.formValue = this.loginForm.value;
      this.authService
        .login(this.formValue.email, this.formValue.password)
        .subscribe(
          data => {
            this.user = data;
            console.log(data);
            if (this.user && this.user.token) {
              this.payload = window.atob(this.user.token.split(".")[1]);
              console.log(this.payload);
              localStorage.setItem("USER", JSON.stringify(this.user.token));
              const decoded = jwtDecode(this.user.token);
              if (decoded) {
                this.authService.updateUser(decoded.id);
                this.router.navigate(["dashboard"]);
              }
            }
          },
          error => {
            console.log(error);
          }
        );
    }
  }
}
