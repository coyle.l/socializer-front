import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "../../shared-service/authentication.service";
import { Router } from "@angular/router";
import * as jwtDecode from "jwt-decode";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted: boolean = false;
  formValue: any;
  user: any;
  payload: any;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(128)
        ]
      ],
      lastname: ["", [Validators.required]],
      firstname: ["", [Validators.required]],
      adresse: ["", []],
      telephone: ["", []]
    });
  }

  get form() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.form.password.errors);
    if (!this.registerForm.invalid) {
      this.formValue = JSON.stringify(this.registerForm.value);
      this.authService.register(this.formValue).subscribe(
        data => {
          this.user = data;
          console.log(data);
          if (this.user && this.user.token) {
            this.payload = window.atob(this.user.token.split(".")[1]);
            console.log(this.payload);
            localStorage.setItem("USER", JSON.stringify(this.user.token));
            const decoded = jwtDecode(this.user.token);
            if (decoded) {
              this.authService.updateUser(decoded.id);
              this.router.navigate(["dashboard"]);
            }
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
