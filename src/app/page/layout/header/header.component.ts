import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/shared-service/authentication.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  connectedUser: any = {};

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
      console.log("header comp connectedUser", user);
      this.connectedUser = user;
    });
  }
  logout() {
    localStorage.removeItem("USER");
    this.authService.updateUser(null);
    this.router.navigate(["home"]);
  }
}
