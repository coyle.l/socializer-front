import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "./shared-service/authentication.service";
import * as jwtDecode from "jwt-decode";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) {}
  ngOnInit(): void {
    const token = localStorage.getItem("USER");
    if (token) {
      const decoded = jwtDecode(token);
      this.authService.updateUser(decoded.id);
    } else {
      this.authService.updateUser(null);
    }
    this.router.navigate(["home"]);
  }
}
